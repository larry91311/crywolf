﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontWallDisappear : Skill
{
        public GameObject player;
    public GameObject wall;

    public GameObject SkillUI;
    List<GameObject> Wall=new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0;i<40;i++)
        {
            Wall.Add(wall.transform.GetChild(i).gameObject);
        }
        
    }
    public override void SkillEffect(){
        if(SkillUI.tag=="Button")
        {
        StartCoroutine(HideFrontWallInSeconds(3));
        }
    }
    
    

    IEnumerator HideFrontWallInSeconds(float time){
        SkillUI.tag="ButtonOn";
        int[] wallfalse=new int[4]{0,0,0,0};

        for(int i=0,j=0;i<40;i++)
        {
            if(Mathf.Abs(Wall[i].transform.position.x-player.transform.position.x)<2.25f)
            {
                if((Wall[i].transform.position.z-player.transform.position.z)>0&&Wall[i].tag=="H")
                {
                    Wall[i].SetActive(false);
                    wallfalse[j]=i;
                    j++;
                }
            }
            
        }

        yield return new WaitForSeconds(time);

        for(int i=0;i<4;i++)
        {
            if(wallfalse[i]!=0)
            {
                Wall[wallfalse[i]].SetActive(true);
            }
        }

        

        
    }
}
