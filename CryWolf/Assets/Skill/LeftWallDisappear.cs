﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftWallDisappear : Skill
{
    public GameObject player;
    public GameObject wall;

    public GameObject SkillUI;
    List<GameObject> Wall=new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0;i<40;i++)
        {
            Wall.Add(wall.transform.GetChild(i).gameObject);
        }
        
    }
    public override void SkillEffect(){
        if(SkillUI.tag=="Button")
        {
        StartCoroutine(HideLeftWallInSeconds(3));
        }
    }
    
    

    IEnumerator HideLeftWallInSeconds(float time){
        SkillUI.tag="ButtonOn";
        int[] wallfalse=new int[4]{0,0,0,0};

        for(int i=0,j=0;i<40;i++)
        {
            if(Mathf.Abs(player.transform.position.z-Wall[i].transform.position.z)<2.25f)
            {
                if((player.transform.position.x-Wall[i].transform.position.x)>0&&Wall[i].tag=="V")
                {
                    Wall[i].SetActive(false);
                    wallfalse[j]=i;
                    j++;
                }
            }
            
        }

        yield return new WaitForSeconds(time);

        for(int i=0;i<4;i++)
        {
            if(wallfalse[i]!=0)
            {
                Wall[wallfalse[i]].SetActive(true);
            }
        }

        

        
    }
}
