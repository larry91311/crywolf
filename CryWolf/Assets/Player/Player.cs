﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController), typeof(PlayerData), typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    PlayerController playerController;
    PlayerData playerData;
    Manager manager;

    

    

    // Start is called before the first frame update
    void Start()
    {
        manager = Manager.instance;
        manager.player = gameObject;
        playerController = GetComponent<PlayerController>();
        playerData = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        
        playerController.Move(new Vector3(Input.GetAxis("Horizontal"), GetComponent<Rigidbody>().velocity.y, Input.GetAxis("Vertical")).normalized * playerData.speed);
        Rotat();

        
    }
    public void Rotat()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical"); 
        Vector3 direction = new Vector3(horizontal, 0, vertical);

        if (direction != Vector3.zero)
        {
        
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);
        }

    }
}
