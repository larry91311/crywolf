﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomNumber : MonoBehaviour
{
    public GameObject player;

    public GameObject RoomNumObject;

    public GameObject state;
    
    // Start is called before the first frame update
    int RoomH;
    int RoomV;
    void Start()
    {
       GetComponent<Text>().text="Press R GET"+"\n"+"ROOMNUMBER";
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if(state.tag=="WOLF")
        {
            RoomNumObject.SetActive(false);
        }
        else
        {
            RoomNumObject.SetActive(true);
            
        }
        
        if(Input.GetKey(KeyCode.R))
        {
            for(int i=-2;i<3;i++)
            {
                for(int j=-2;j<3;j++)
                {
                    if(player.transform.position.x<=i*5+2.25f)
                    {
                        if(player.transform.position.x>=i*5-2.25f)
                        {
                            if(player.transform.position.z<=j*5+2.25f)
                            {
                                if(player.transform.position.z>=j*5-2.25f)
                                {
                                    RoomV=i+2;
                                    RoomH=j+2;

                                    GetComponent<Text>().text="RoomNumber:"+RoomH+RoomV;


                                }
                            }

                        }
                    }


                }
            }
        }
        
    }
}
