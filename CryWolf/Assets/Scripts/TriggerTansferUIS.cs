﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTansferUIS : MonoBehaviour
{
    public GameObject TransferUIS;
    public GameObject WolfTransfer;

    public GameObject TransferUI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag=="Player"&&TransferUI.tag=="TransferButton")
        {
        TransferUIS.SetActive(true);
        }
        if(other.tag=="Wolf"&&WolfTransfer.tag=="WolfNot")
        {
        TransferUIS.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.tag=="Player")
        {
        TransferUIS.SetActive(false);
        }
        if(other.tag=="Wolf")
        {
        TransferUIS.SetActive(false);
        }
    }
}
