﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WolfDetect : MonoBehaviour
{
    public Image DetectUI;
    public GameObject DetectObject;

    public GameObject player;
    public GameObject wolf;

    public GameObject state;
    // Start is called before the first frame update
    void Start()
    {
        DetectUI.color=Color.white;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
        if(Mathf.Sqrt((player.transform.position.x-wolf.transform.position.x)*(player.transform.position.x-wolf.transform.position.x)+(player.transform.position.z-wolf.transform.position.z)*(player.transform.position.z-wolf.transform.position.z))<5)
        {
            DetectUI.color=Color.black;
        }
        else if(Mathf.Sqrt((player.transform.position.x-wolf.transform.position.x)*(player.transform.position.x-wolf.transform.position.x)+(player.transform.position.z-wolf.transform.position.z)*(player.transform.position.z-wolf.transform.position.z))<10)
        {
            DetectUI.color=Color.red;
        }
        else if(Mathf.Sqrt((player.transform.position.x-wolf.transform.position.x)*(player.transform.position.x-wolf.transform.position.x)+(player.transform.position.z-wolf.transform.position.z)*(player.transform.position.z-wolf.transform.position.z))<15)
        {
            DetectUI.color=Color.green;
        }
        else if(Mathf.Sqrt((player.transform.position.x-wolf.transform.position.x)*(player.transform.position.x-wolf.transform.position.x)+(player.transform.position.z-wolf.transform.position.z)*(player.transform.position.z-wolf.transform.position.z))<20)
        {
            DetectUI.color=Color.yellow;
        }
        else if(Mathf.Sqrt((player.transform.position.x-wolf.transform.position.x)*(player.transform.position.x-wolf.transform.position.x)+(player.transform.position.z-wolf.transform.position.z)*(player.transform.position.z-wolf.transform.position.z))<35)
        {
            DetectUI.color=Color.white;
        }
        if(state.tag=="PLAYER")
        {
            DetectUI.color=Color.gray;
        }

    }
}
