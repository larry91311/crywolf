﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTransferUID : MonoBehaviour
{
    public GameObject TransferUID;
    public GameObject TransferUI;

    public GameObject WolfTransfer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag=="Player"&&TransferUI.tag=="TransferButton")
        {
        TransferUID.SetActive(true);
        }
        if(other.tag=="Wolf"&&WolfTransfer.tag=="WolfNot")
        {
        TransferUID.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other) {
       if(other.tag=="Player")
        {
        TransferUID.SetActive(false);
        }
        if(other.tag=="Wolf")
        {
        TransferUID.SetActive(false);
        }
    }
}
