﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTransferUIA : MonoBehaviour
{
    public GameObject TransferUIA;
    public GameObject TransferUI;

    public GameObject WolfTransfer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) {
        
       if(other.tag=="Player"&&TransferUI.tag=="TransferButton")
        {
        TransferUIA.SetActive(true);
        }
        if(other.tag=="Wolf"&&WolfTransfer.tag=="WolfNot")
        {
        TransferUIA.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.tag=="Player")
        {
        TransferUIA.SetActive(false);
        }
        if(other.tag=="Wolf")
        {
        TransferUIA.SetActive(false);
        }
    }
}
