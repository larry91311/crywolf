﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameEvent : MonoBehaviour
{
    public float TimePeriod=10.0f;
    public GameObject playercam, wolfcam; 
    public GameObject player, wolf; 

    public GameObject SkillUI;
    public GameObject TransferUI;

    public GameObject WolfTransfer;

    public GameObject TimeText;

    public GameObject TransferButtonS;
    public GameObject TransferButtonA;
    public GameObject TransferButtonD;
    public GameObject TransferButtonW;

    
    public GameObject DisappearButtonS;
    public GameObject DisappearButtonA;
    public GameObject DisappearButtonD;
    public GameObject DisappearButtonW;

   public GameObject state;

    

    public Rigidbody playerRigidBody;
    public Rigidbody wolfRigidBody;
    private float timer=0.0f;

    private bool EndGame=false;
    void Awake ()
    {
        

        
        wolfcam.SetActive(false);
        playercam.SetActive(true);
        InitialPosition();

        
        
    }
    
    void Update ()
    {
        if(timer==0.0f)
        {
            StartCoroutine(SkillTime(TimePeriod));  //前十秒技能階段，標籤設置
            StartCoroutine(PlayerTime(TimePeriod*2)); //玩家未移動隨機移動
             StartCoroutine(WolfTime(TimePeriod*2));


             StartCoroutine(WolfEndTime(TimePeriod*3));
             StartCoroutine(PlayerEndTime(TimePeriod*2));


            
           
            
           
            
        }
        if(timer<TimePeriod) //技能時間限制
        {
            DisappearButtonA.SetActive(true);
            DisappearButtonW.SetActive(true);
            DisappearButtonS.SetActive(true);
            DisappearButtonD.SetActive(true);

            state.tag="PLAYER";
            playerRigidBody.constraints= RigidbodyConstraints.FreezePositionZ|RigidbodyConstraints.FreezePositionX|RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
        //玩家位置鎖住

            wolfRigidBody.constraints= RigidbodyConstraints.FreezePositionZ|RigidbodyConstraints.FreezePositionX|RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
        //狼位置鎖住


        }
        else if(timer<TimePeriod*2&&timer>TimePeriod)  //玩家時間限制
        {
            playerRigidBody.constraints= RigidbodyConstraints.None;
            playerRigidBody.constraints= RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
            //人自由移動
            state.tag="PLAYER";

        }
        else if(timer<TimePeriod*3&&timer>TimePeriod*2)    //狼時間限制
        {
            state.tag="WOLF";
            wolfcam.SetActive(true);
            playercam.SetActive(false);

            wolfRigidBody.constraints= RigidbodyConstraints.None;
            wolfRigidBody.constraints= RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
            //狼移動解鎖

            playerRigidBody.constraints= RigidbodyConstraints.FreezePositionZ|RigidbodyConstraints.FreezePositionX|RigidbodyConstraints.FreezePositionY| RigidbodyConstraints.FreezeRotationX| RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
            //人移動限制
            DisappearButtonA.SetActive(false);
            DisappearButtonW.SetActive(false);
            DisappearButtonS.SetActive(false);
            DisappearButtonD.SetActive(false);

        }
        
        timer+=Time.deltaTime;
        
        
        if(timer>TimePeriod)
        {
            
            TimeText.GetComponent<Text>().text="PlayerTime:"+timer.ToString();
            
            
                
            if(timer>TimePeriod*2)
            {
                TimeText.GetComponent<Text>().text="WolfTime:"+timer.ToString();
                if(timer>TimePeriod*3)
                {
                timer=0;
                }
            }

        }
        else
        {
            
            TimeText.GetComponent<Text>().text="SkillTime:"+timer.ToString();
        }
        
    }

    IEnumerator SkillTime(float time){
       SkillUI.tag="Button";
       TransferUI.tag="TransferButton";
       
       
       

        yield return new WaitForSeconds(time);
        TransferButtonS.SetActive(false);
        TransferButtonA.SetActive(false);
        TransferButtonD.SetActive(false);
        TransferButtonW.SetActive(false);

        

        

        

        

        
    }
    IEnumerator WolfTime(float time){
      
       
       

        yield return new WaitForSeconds(time);
        WolfTransfer.tag="WolfNot";

        

        

        

        

        
    }
    IEnumerator PlayerEndTime(float time){
      
       
       

        yield return new WaitForSeconds(time);
       TransferButtonS.SetActive(false);
        TransferButtonA.SetActive(false);
        TransferButtonD.SetActive(false);
        TransferButtonW.SetActive(false);

        

        

        

        

        
    }
    IEnumerator WolfEndTime(float time){
      
       
       

        yield return new WaitForSeconds(time);
       TransferButtonS.SetActive(false);
        TransferButtonA.SetActive(false);
        TransferButtonD.SetActive(false);
        TransferButtonW.SetActive(false);

        

        

        

        

        
    }
    IEnumerator PlayerTime(float time){    //未過傳送們隨機移動
       
       
       yield return new WaitForSeconds(time);
       
        if(TransferUI.tag=="TransferButton")
        {
            if(player.transform.position.x<7.25f&&player.transform.position.x>-7.25f&&player.transform.position.z<7.25f&&player.transform.position.z>7.25f)
            {
                    int randNum=0;
                    randNum=Random.Range(0,3);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,0f);
                            break;
                            case 2:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            case 3:
                            player.transform.position+=new Vector3(5.0f,0f,0f);
                            break;

                    
                    
                            default:
                            player.transform.position+=new Vector3(5.0f,0f,0f);
                            break;
                    }
            }
            else if(player.transform.position.x>7.75f&&player.transform.position.z>7.75f) //右上角
            {
                int randNum=0;
                    randNum=Random.Range(0,1);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.x<-7.75f&&player.transform.position.z>7.75f) //左上角
            {
                int randNum=0;
                    randNum=Random.Range(0,1);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(5.0f,0f,0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.x<-7.75f&&player.transform.position.z<-7.75f) //左下角
            {
                int randNum=0;
                    randNum=Random.Range(0,1);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(5.0f,0f,0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.x>7.75f&&player.transform.position.z<-7.75f) //右下角
            {
                int randNum=0;
                    randNum=Random.Range(0,1);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.x>7.75f) //最右外邊
            {
                int randNum=0;
                    randNum=Random.Range(0,2);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,0f);
                            break;
                            case 2:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.x<7.75f) //最左外邊
            {
                int randNum=0;
                    randNum=Random.Range(0,2);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(5.0f,0f,0.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            case 2:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.z>7.75f) //最上外邊
            {
                int randNum=0;
                    randNum=Random.Range(0,2);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(5.0f,0f,0.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,5.0f);
                            break;
                            case 2:
                            player.transform.position+=new Vector3(0f,0f,-5.0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }
            else if(player.transform.position.z<7.75f) //最下外邊
            {
                int randNum=0;
                    randNum=Random.Range(0,2);
                    switch(randNum)
                    {
                            case 0:
                            player.transform.position+=new Vector3(5.0f,0f,0.0f);
                            break;
                            case 1:
                            player.transform.position+=new Vector3(-5.0f,0f,5.0f);
                            break;
                            case 2:
                            player.transform.position+=new Vector3(0f,0f,5.0f);
                            break;
                            
                            

                    
                    
                           
                    }

            }

        }
        
                
            

        

        

        

        

        
    }

    void InitialPosition()
    {
        player.transform.position=new Vector3((int)Random.Range(-2,2)*5,player.transform.position.y,(int)Random.Range(-2,2)*5);
        wolf.transform.position=new Vector3((int)Random.Range(-2,2)*5,wolf.transform.position.y,(int)Random.Range(-2,2)*5);

        if(player.transform.position.x-wolf.transform.position.x==0&&player.transform.position.z-wolf.transform.position.z==0)
        {
            InitialPosition();

        }
        
    }
    
}
