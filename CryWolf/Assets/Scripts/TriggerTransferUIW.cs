﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TriggerTransferUIW : MonoBehaviour
{
    public GameObject TransferUIW;
    public GameObject TransferUI;

    public GameObject WolfTransfer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) {
       if(other.tag=="Player"&&TransferUI.tag=="TransferButton")
        {
        TransferUIW.SetActive(true);
        }
        if(other.tag=="Wolf"&&WolfTransfer.tag=="WolfNot")
        {
        TransferUIW.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other) {
       if(other.tag=="Player")
        {
        TransferUIW.SetActive(false);
        }
        if(other.tag=="Wolf")
        {
        TransferUIW.SetActive(false);
        }
    }
   
}
