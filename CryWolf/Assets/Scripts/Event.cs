﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Event : MonoBehaviour
{
    public GameObject player;

    public GameObject wolf;
    public GameObject transferUI;

    public GameObject WolfTransfer;

    public GameObject state;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        
    }
    public void TransferW()
    {
        transferUI.tag="TransferButtonOn";
        WolfTransfer.tag="WolfDid";
        if(state.tag=="PLAYER")
        {
        player.transform.position+=new Vector3(0f,0f,3.0f);
        }
        else 
        {
            wolf.transform.position+=new Vector3(0f,0f,3.0f);
        }
        

    }
    public void TransferA()
    {
        transferUI.tag="TransferButtonOn";
        WolfTransfer.tag="WolfDid";
        if(state.tag=="PLAYER")
        {
        player.transform.position+=new Vector3(-3.0f,0f,0.0f);
        }
        else
        {
            wolf.transform.position+=new Vector3(-3.0f,0f,0.0f);
        }
        
       
    }
    public void TransferS()
    {
        transferUI.tag="TransferButtonOn";
        WolfTransfer.tag="WolfDid";
        if(state.tag=="PLAYER")
        {
        player.transform.position+=new Vector3(0f,0f,-3.0f);
        }
        else
        {
            wolf.transform.position+=new Vector3(0f,0f,-3.0f);
        }
        
    }
    public void TransferD()
    {
        transferUI.tag="TransferButtonOn";
        WolfTransfer.tag="WolfDid";
        if(state.tag=="PLAYER")
        {
        player.transform.position+=new Vector3(3.0f,0f,0f);
        }
        else
        {
           wolf.transform.position+=new Vector3(3.0f,0f,0f);
        }
        
    }
    
}
