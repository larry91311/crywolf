﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDisappear : MonoBehaviour
{
    public GameObject player;
    public GameObject wall;
    List<GameObject> Wall=new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0;i<40;i++)
        {
            Wall.Add(wall.transform.GetChild(i).gameObject);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisappearW()
    {
        for(int i=0;i<40;i++)
        {
            if(Mathf.Abs(Wall[i].transform.position.x-player.transform.position.x)<2.25f)
            {
                if((Wall[i].transform.position.z-player.transform.position.z)>0&&Wall[i].tag=="H")
                {
                    Wall[i].SetActive(false);
                }
            }
            
        }

    }
    public void DisappearA()
    {
        for(int i=0;i<40;i++)
        {
            if(Mathf.Abs(player.transform.position.z-Wall[i].transform.position.z)<2.25f)
            {
                if((player.transform.position.x-Wall[i].transform.position.x)>0&&Wall[i].tag=="V")
                {
                    Wall[i].SetActive(false);
                }
            }
            
        }
    }
    public void DisappearS()
    {
        for(int i=0;i<40;i++)
        {
            if(Mathf.Abs(player.transform.position.x-Wall[i].transform.position.x)<2.25f)
            {
                if((player.transform.position.z-Wall[i].transform.position.z)>0&&Wall[i].tag=="H")
                {
                    Wall[i].SetActive(false);
                }
            }
            
        }
    }
    public void DisappearD()
    {
         for(int i=0;i<40;i++)
        {
            if(Mathf.Abs(player.transform.position.z-Wall[i].transform.position.z)<2.25f)
            {
                if((Wall[i].transform.position.x-player.transform.position.x)>0&&Wall[i].tag=="V")
                {
                    Wall[i].SetActive(false);
                }
            }
            
        }
    }
}
