﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
     static Manager _instance;
    public static Manager instance{
        get{
            if (_instance == null){
                _instance = FindObjectOfType(typeof(Manager)) as Manager;
                if (_instance == null){
                    GameObject go = new GameObject("Manager");
                    _instance = go.AddComponent<Manager>();
                }
            }
            return _instance;
        }
    }

    public GameObject player;
    
}
