﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCard : MonoBehaviour
{
    public void OnClick(){
        GetComponent<Skill>().SkillEffect();
    }
}
