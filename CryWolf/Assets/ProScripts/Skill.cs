﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour
{
   public Manager manager;

    private void Start() {
        manager = Manager.instance;    
    }

    public virtual void SkillEffect(){}
}
