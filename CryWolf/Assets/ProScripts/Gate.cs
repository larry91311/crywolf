﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
   public static Vector3 currentDestination = new Vector3();
    public Vector3 destination;

    public GameObject transferUI;
    
    public void ShowTransferUI(){
        currentDestination = destination;
        transferUI.SetActive(true);
    }

    public void HideTransferUI(){
        transferUI.SetActive(false);
    }
}
